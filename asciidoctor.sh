#!/bin/sh
ln -s ./templates/images/*.png . &&
    asciidoctor *.adoc --section-numbers \
        --attribute sectlinks="" \
        --attribute source-highlighter="rouge" \
        --attribute toc="left" \
        --attribute favicon= \
        --attribute icons=font \
        --attribute lang=pt_BR \
        --attribute allow-uri-read= \
        --attribute stem= \
        --attribute experimental= \
        --attribute imagesdir@=images \
        -o templates/index.html
